from setuptools import Extension, setup
from Cython.Build import cythonize

sourcefiles = ['datastructures.pyx', 'functions.pyx']

setup(
    ext_modules=cythonize(sourcefiles)
)
