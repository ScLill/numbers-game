operations = ('+', '-', '*', '/')

cdef class TreeBase:
    def __repr__(self):
        return f'{self.expr(None, None)} = {self.value}'

    def __lt__(self, other):
        return self.numNodes() < other.numNodes()

cdef class TreeLeaf(TreeBase):
    def __init__(self, value: int):
        self.value = value

    def __eq__(self, other):
        return self.value == other.value

    def __hash__(self):
        return hash(self.value)

    @property
    def op(self):
        return None

    def expr(self, leftAncestor, rightAncestor):
        return str(self.value)

    def numNodes(self):
        return 1

cdef class TreeNode(TreeBase):
    _left: TreeBase
    _right: TreeBase
    _op: str

    def __init__(TreeNode self, TreeBase left, str op, TreeBase right):
        self._left = left
        self._op = op
        self._right = right

        self.value = self.calcValue()

    cdef int calcValue(TreeNode self):
        if self.op == '+':
            return self.left.value + self.right.value
        elif self.op == '-':
            return self.left.value - self.right.value
        elif self.op == '*':
            return self.left.value * self.right.value
        elif self.op == '/':
            if self.left.value % self.right.value != 0:
                raise ArithmeticError(f'Invalid integer division, {self.right.value} does not divide {self.left.value}')
            return self.left.value // self.right.value

    def __eq__(self, other):
        return (self.left == other.left and
                self.op == other.op and
                self.right == other.right)

    def __hash__(self):
        return hash((self.left, operations.index(self.op), self.right))

    @property
    def left(self):
        return self._left

    @property
    def op(self):
        return self._op

    @property
    def right(self):
        return self._right

    def expr(self, leftAncestor, rightAncestor):
        if self.op == '+':
            acceptableLeftAncestors = ['+']
            acceptableRightAncestors = ['+', '-']
        elif self.op == '*':
            acceptableLeftAncestors = ['+', '-', '*']
            acceptableRightAncestors = ['+', '-', '*', '/']
        elif self.op == '-':
            acceptableLeftAncestors = ['+']
            acceptableRightAncestors = ['+', '-']
        elif self.op == '/':
            acceptableLeftAncestors = ['+', '-']
            acceptableRightAncestors = ['+', '-']

        checkLeft = leftAncestor is None or leftAncestor.op in acceptableLeftAncestors
        checkRight = rightAncestor is None or rightAncestor.op in acceptableRightAncestors
        passLeft = leftAncestor if checkLeft else None
        passRight = rightAncestor if checkRight else None

        baseExpr = f'{self.left.expr(passLeft, self)} {self.op} {self.right.expr(self, passRight)}'
        if checkLeft and checkRight:
            return baseExpr
        else:
            return f'({baseExpr})'

    def numNodes(self):
        return self.left.numNodes() + 1 + self.right.numNodes()
