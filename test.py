#!/usr/bin/env python

import sys
sys.path.append('build/lib.linux-x86_64-cpython-312/')

from functions import expressions, solve
import unittest

class TestExpression(unittest.TestCase):
    def baseTest(self, numbers, answers):
        exps = {repr(exp) for exp in expressions(numbers)}

        for exp in exps:
            self.assertIn(exp, answers)

        for answer in answers:
            self.assertIn(answer, exps)

    def test1(self):
        numbers = [1, 1]
        answers = {'1 - 1 = 0', '1 + 1 = 2', '1 = 1'}

        self.baseTest(numbers, answers)

    def test2(self):
        numbers = [1, 1, 2]
        answers = {'(1 + 1) / 2 = 1', '2 - 1 - 1 = 0', '1 + 1 = 2', '1 = 1', '(1 + 1) * 2 = 4', 
            '1 - 1 = 0', '(1 - 1) / 2 = 0', '2 + 1 - 1 = 2', '2 + 1 = 3', '2 - 1 = 1', '2 + 1 + 1 = 4', '2 = 2', '2 / (1 + 1) = 1'}

        self.baseTest(numbers, answers)

class TestSolve(unittest.TestCase):
    def baseTest(self, numbers, target, answers):
        solutions = solve(numbers, target)
        solutions = [repr(sol) for sol in solutions]

        answers.sort()
        solutions.sort()

        for answer, solution in zip(answers, solutions):
            self.assertEqual(answer, solution)

    def test848(self):
        numbers = [50, 75, 4, 7, 6, 1]
        target = 848

        answers = ['(75 + 50 - 4) * 7 + 1 = 848',
            '(75 + 50 - 4 + 1) * 7 - 6 = 848',
            '75 * 7 + (50 + 4) * 6 - 1 = 848',
            '((75 - 50) * 4 + 6) * (7 + 1) = 848',
            '(50 * 7 + 75 - 1) * (6 - 4) = 848',
            '(75 + 6) * (7 + 1) + 50 * 4 = 848',
            '((50 - 1) * 6 - 75 - 7) * 4 = 848',
            '(75 + 1) * 7 * 6 / 4 + 50 = 848']

        self.baseTest(numbers, target, answers)

    def test707(self):
        numbers = [25, 8, 6, 1, 1, 7]
        target = 707

        answers = ['((25 - 8) * 6 - 1) * 7 = 707',
            '((25 - 8) * 7 - 1) * 6 - 1 = 707',
            '25 * (8 + 6) * (1 + 1) + 7 = 707']

        self.baseTest(numbers, target, answers)

    def test659(self):
        numbers = [5, 8, 3, 2, 7, 0]
        target = 659

        answers = ['(8 * 2 + 3) * 7 * 5 = 665']
        self.baseTest(numbers, target, answers)


if __name__ == "__main__":
    unittest.main()
