cimport datastructures
from datastructures import *

from itertools import *
import argparse

def main():
    parser = argparse.ArgumentParser(description='Numbers game solver', usage='%(prog)s: target num1 num2 num3 ...')
    parser.add_argument('target', type=int,help='Target to reach')
    parser.add_argument('numbers', nargs='+', type=int, help='Selection of large and small numbers')

    args = parser.parse_args()
    target = args.target
    numbers = args.numbers

    s = solve(numbers, target)
    for solution in s:
        print(solution)

def selectNumbers(numbers, minSize=None, maxSize=None):
    if minSize is None:
        minSize = 0
    if maxSize is None:
        maxSize = len(numbers)
    return chain.from_iterable(combinations(numbers, r) for r in range(minSize, maxSize+1))

def doSubTasks(args):
    subTasks, numsToExpressions = args
    subNumsToExpressions = {}
    for task in subTasks:
        selection, leftNums, rightNums = task
        leftExprs = numsToExpressions[leftNums]
        rightExprs = numsToExpressions[rightNums]

        for leftExpr, operation, rightExpr in product(leftExprs, operations, rightExprs):
            if not prune(leftExpr, operation, rightExpr):
                expression = TreeNode(leftExpr, operation, rightExpr)
                if selection not in subNumsToExpressions:
                    subNumsToExpressions[selection] = []
                subNumsToExpressions[selection].append(expression)
    return subNumsToExpressions

def expressions(numbers):
    numsToExpressions = {}
    for num in numbers:
        numsToExpressions[(num,)] = [TreeLeaf(num)]

    for n in range(2, len(numbers)+1):
        tasks = set()
        for selection in combinations(numbers, n):
            numsToExpressions[selection] = list()

            for leftNums in selectNumbers(selection, 1, n-1):
                rightNums = list(selection)
                for num in leftNums:
                    rightNums.remove(num)
                rightNums = tuple(rightNums)

                tasks.add((selection, leftNums, rightNums))

        tasks = tuple(tasks)
        args = tuple((tasks[i*10:(i+1)*10], numsToExpressions) for i in range(len(tasks)//10 + 1))

        results = []
        for arg in args:
            result = doSubTasks(arg)
            results.append(result)

        for subNumsToExpressions in results:
            for selection in subNumsToExpressions:
                numsToExpressions[selection].extend(subNumsToExpressions[selection])

    for expressions in numsToExpressions.values():
        yield from expressions
    
def solve(numbers, target):
    bestError = None
    for expression in expressions(numbers):
        error = expression.value - target
        if error < 0:
            error = -error
    
        if bestError is None or error < bestError:
            answers = set()

        if bestError is None or error <= bestError:
            answers.add(expression)
            bestError = error

    answers = list(answers)
    answers.sort()
    return answers

cdef bint lessThanWithHash(datastructures.TreeBase left, datastructures.TreeBase right):
    return (left.value < right.value or
        (left.value == right.value and hash(left) < hash(right)))

cdef bint prune(datastructures.TreeBase left, str op, datastructures.TreeBase right):
    # Conditions check if the expression fits the pattern on the left hand
    # side of the equation in the attached comment. They can be pruned
    # because they are covered by the cases on right hand side of the
    # equation. In cases that only move numbers, and leave all operations
    # and parantheses in place, we keep the case when the larger number
    # comes first e.g. (5 + 1) is kept and (1 + 5) is pruned. If different
    # expressions have the same value, prune is based on hash instead

    cdef bint additionPrune = op == '+' and (
        # (a + b) == (b + a)
        # s.t. a < b
        lessThanWithHash(left, right) or

        # (a + (b + c)) == ((a + b) + c)
        # (a + (b - c)) == ((a + b) - c)
        right.op in ('+', '-') or

        # ((a + b) + c) == ((a + c) + b)
        # ((a - b) + c) == ((a - c) + a)
        # s.t. b < c
        (left.op in ('+' ,'-') and lessThanWithHash(left.right, right)) or

        # 0 + a == a
        left.value == 0 or
        # a + 0 == a
        right.value == 0
    )

    cdef bint subtractionPrune = op == '-' and (
        # (a - (b + c)) == ((a - b) - c)
        # (a - (b - c)) == ((a - b) + c)
        right.op in ('+', '-') or

        # ((a + b) - c) == ((a - c) + b)
        # ((a - b) - c) == ((a - c) - b)
        # s.t. b < c
        (left.op in ('+', '-') and lessThanWithHash(left.right, right)) or

        # a - 0 == a
        right.value == 0 or

        # Only search for positive numbers. They can be subtracted later if needed
        # (a - b) == -(b - a)
        left.value - right.value < 0
    )

    cdef bint multiplyPrune = op == '*' and (
        # (a * b) == (b * a)
        lessThanWithHash(left, right) or

        # (a * (b * c)) == ((a * b) * c)
        # (a * (b / c)) == ((a * b) / c)
        right.op in ('*', '/') or

        # ((a * b) * c) == ((a * c) * b)
        # ((a / b) * c) == ((a * c) / b)
        # s.t. b < c
        (left.op in ('*', '/') and lessThanWithHash(left.right, right)) or

        # 0 * a == 0
        left.value == 0 or
        # a * 0 == 0
        right.value == 0 or
        # 1 * a == a
        left.value == 1 or
        # a * 1 == a
        right.value == 1
    )

    cdef bint divisionPrune = op == '/' and (
        # (a / (b * c)) == ((a / b) / c)
        # (a / (b / c)) == ((a / b) * c)
        right.op in ('*', '/') or

        # ((a * b) / c) == ((a / c) * b)
        # ((a / b) / c) == ((a / c) / b)
        # s.t. b < c
        # Note: due to integer division, a % c == 0 is required
        (left.op in ('*', '/') and right.value != 0 and
         left.left.value % right.value == 0 and lessThanWithHash(left.right, right)) or

        # a / 1 == a
        right.value == 1 or

        # Illegal division
        right.value == 0 or
        left.value % right.value != 0
    )

    return additionPrune or subtractionPrune or multiplyPrune or divisionPrune
